var boot = function () {};

boot.prototype = {

    loadScripts: function () {
        game.load.script('WebFont', 'lib/webfontloader.js');
        game.load.script('gameMenu','states/gameMenu.js');
        game.load.script('credit','states/credits.js');
        game.load.script('playGame','states/playGame.js');
        game.load.script('scores','states/scores.js');
        game.load.script('settings','states/settings.js');
        game.load.script('gameStages','states/gameStages.js');
    },

    loadBgm: function () {
        game.load.audio('intromusic', ['assets/bgm/intro.mp3', 'assets/bgm/intro.ogg']);
        game.load.audio('menumusic', ['assets/bgm/menumusic.mp3', 'assets/bgm/menumusic.ogg']);
        game.load.audio('gameovermusic', ['assets/bgm/gameovermusic.mp3', 'assets/bgm/gameovermusic.ogg']);  
        game.load.audio('clickmusic', ['assets/bgm/clickmusic.mp3', 'assets/bgm/clickmusic.ogg']); 
    },

    loadImages: function () {
        //game.load.image('image-id', 'assets/images/menu-name.jpg');
        //sprite width = 512/8=64, height = 576/9=64, total animations = 65
        //game.load.spritesheet('icons', 'assets/images/icons_white2x.png', 105, 105);
        game.load.spritesheet('icons', 'assets/images/icons.png', 100, 100, 120);
    },

    loadFonts: function () {
        WebFontConfig = {
            custom: {
                families: ['TheMinion'],
                urls: ['assets/style/theminion.css']
            }
        }
    },

    init: function () {
        this.logo = game.make.sprite(game.world.centerX, 300, 'brand');
        this.logo.anchor.setTo(0.5, 0.5);
        this.status = game.make.text(game.world.centerX, 400, 'Loading...', {fill: 'white'});
        this.status.anchor.setTo(0.5, 0.5);
        this.loadingBar = game.make.sprite(game.world.centerX, 450, "loading");
        this.loadingBar.anchor.setTo(0.5, 0.5);
    },

    preload: function () {
       game.stage.backgroundColor = "#4488AA";
       game.add.existing(this.logo).scale.setTo(0.5);
       game.add.existing(this.loadingBar);
       game.add.existing(this.status);
       this.load.setPreloadSprite(this.loadingBar);

       this.loadScripts();
       this.loadImages();
       this.loadFonts();
       this.loadBgm();

    },

    addGameStates: function () {
        game.state.add('gameMenu', gameMenu);
    },

    addGameMusic: function () {
        
        intromusic = game.add.audio('intromusic');
        intromusic.play();
        
        music = game.add.audio('menumusic');
        music.loop = true;
        music.play();

        clickmusic = game.add.audio('clickmusic');
    },

    create: function() {
        this.status.setText('Ready!');
        this.loadingBar.tint = 0xf00f00;
        this.addGameStates();
        this.addGameMusic();
        setTimeout(function () {
            game.state.start('gameMenu');
        }, 500);
    }
};