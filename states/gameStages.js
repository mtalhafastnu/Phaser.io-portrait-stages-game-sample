var gameStages = function(game) {
    this.holdicons = [];
};

gameStages.prototype = {
    init: function () {
        this.titleText = game.make.text(game.world.centerX, 100, "Select to Play", {
            font: 'bold 50pt TheMinion',
            fill: '#ff8000',
            align: 'center',
            stroke: "green",
            strokeThickness : 10,
        });
        this.titleText.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
        this.titleText.anchor.set(0.5);
        game.add.tween(this.titleText.scale).to({ x: 0.8, y: 0.8}, 700, Phaser.Easing.Linear.None, true, 500, 20, true);    
       
        this.homeIcon = this.add.sprite(0, 0, 'icons', 99);
        this.homeIcon.inputEnabled = true;
        this.homeIcon.events.onInputUp.add(this.clickMusic, this);
        this.homeIcon.events.onInputUp.add(function () {
            game.state.start("gameMenu");   
        });
        this.optionCount = 1;
    },
    create: function () {
        game.add.existing(this.titleText);
        this.createStages();
        this.animateStages();
        game.state.add('playGame', playGame);
    },
    clickMusic: function(){
        clickmusic.stop();
        if(gameOptions.playSound == true){
            if (game.cache.isSoundDecoded('clickmusic')){
                clickmusic.play();
            }

        }
    },
    createStages: function() {
        var levelnr = 0;
        
        for (var y = 0; y < 3; y++) {
            for (var x = 0; x < 4; x++) {
                // next level
                levelnr = levelnr + 1;

                // check if array not yet initialised
                if (typeof PLAYER_DATA[levelnr - 1] !== 'number') {
                    // value is null or undefined, i.e. array not defined or too short between app upgrades with more levels
                    if (levelnr === 1) {
                        PLAYER_DATA[levelnr - 1] = 0; // level 1 should never be locked
                    } else {
                        PLAYER_DATA[levelnr - 1] = -1;
                    }
                }

                // player progress info for this level
                var playdata = PLAYER_DATA[levelnr - 1];
                
                // decide which icon
                var isLocked = true; // locked
                var stars = 0; // no stars
                
                // check if level is unlocked
                if (playdata > -1) {
                    isLocked = false; // unlocked
                    if (playdata < 4) {
                        stars = playdata;
                    }// 0..3 stars
                }

               // calculate position on screen
                var xpos = 100 + (x * 150);
                var ypos = 300 + (y * 150);

                // create icon
                this.holdicons[levelnr - 1] = this.createStageIcon(xpos, ypos, levelnr, isLocked, stars);
                var backicon = this.holdicons[levelnr - 1].getAt(0);

                // keep level nr, used in onclick method
                backicon.health = levelnr;

                // input handler
                backicon.inputEnabled = true;
                backicon.events.onInputDown.add(this.onIconDown, this);
            }
        }
    },
    // -------------------------------------
    // Add stage icon buttons
    // -------------------------------------
    createStageIcon: function (xpos, ypos, levelnr, isLocked, stars) {

        // create new group
        var IconGroup = this.game.add.group();
        IconGroup.x = xpos;
        IconGroup.y = ypos;

        // keep original position, for restoring after certain tweens
        IconGroup.xOrg = xpos;
        IconGroup.yOrg = ypos;

        // determine background frame
        var frame = 0;
        if (isLocked == false) {
            frame = 25;
        }else{
            frame = 33;
        }

        // add background
        var icon1 = this.game.add.sprite(0, 0, 'icons', frame);
        icon1.scale.setTo(1.5);
        IconGroup.add(icon1);

        // add stars, if needed
        if (isLocked == false) {
            var txtstyle = { font: "40px Arial", fill: "#2e161d", align: "center", fontWeight: "bold" };
            var txt = game.add.text(70, 70, levelnr, txtstyle);
            txt.anchor.set(0.5);
            
            var icon2 = this.game.add.sprite(10, 100, 'icons', 61);
            var icon3 = this.game.add.sprite(50, 100, 'icons', 61);
            var icon4 = this.game.add.sprite(90, 100, 'icons', 61);
            icon2.scale.setTo(0.5);
            icon3.scale.setTo(0.5);
            icon4.scale.setTo(0.5);
            
            icon2.tint = 0x2e161d;
            icon3.tint = 0x2e161d;
            icon4.tint = 0x2e161d;
            if(stars === 1){
                icon2.tint = 0xffc125;
            }else if(stars === 2){
                icon2.tint = 0xffc125;
                icon3.tint = 0xffc125;
            }
            else if(stars === 3){
                icon2.tint = 0xffc125;
                icon3.tint = 0xffc125;
                icon4.tint = 0xffc125;
            }
            
  
            IconGroup.add(txt);
            IconGroup.add(icon2);
            IconGroup.add(icon3);
            IconGroup.add(icon4);
        }

        return IconGroup;
    },
    onIconDown: function (sprite, pointer) {
        this.clickMusic();
        // retrieve the iconlevel
        var levelnr = sprite.health;

        if (PLAYER_DATA[levelnr - 1] < 0) {
            // indicate it's locked by shaking left/right
            var IconGroup = this.holdicons[levelnr - 1];
            var xpos = IconGroup.xOrg;

            var tween = this.game.add.tween(IconGroup)
                    .to({x: xpos + 6}, 20, Phaser.Easing.Linear.None)
                    .to({x: xpos - 5}, 20, Phaser.Easing.Linear.None)
                    .to({x: xpos + 4}, 20, Phaser.Easing.Linear.None)
                    .to({x: xpos - 3}, 20, Phaser.Easing.Linear.None)
                    .to({x: xpos + 2}, 20, Phaser.Easing.Linear.None)
                    .to({x: xpos}, 20, Phaser.Easing.Linear.None)
                    .start();
        } else {
            // simulate button press animation to indicate selection
            var IconGroup = this.holdicons[levelnr - 1];
            var tween = this.game.add.tween(IconGroup.scale)
                    .to({x: 0.9, y: 0.9}, 100, Phaser.Easing.Linear.None)
                    .to({x: 1.0, y: 1.0}, 100, Phaser.Easing.Linear.None)
                    .start();
            
            //on click update the selected stage value
            gameOptions.gameStage = levelnr;
            tween.onComplete.add(function(){
                game.state.start("playGame");
            });
        }
    },
    animateStages: function () {
        
        // slide all icons into screen
        for (var i = 0; i < this.holdicons.length; i++) {
            // get variables
            var IconGroup = this.holdicons[i];
            IconGroup.y = IconGroup.y + 600;
            var y = IconGroup.y;

            // tween animation
            this.game.add.tween(IconGroup).to({y: y - 600}, 500, Phaser.Easing.Back.Out, true, (i * 40));
        }
    },
    clickMusic: function(){
        clickmusic.stop();
        if(gameOptions.playSound == true){
            if (game.cache.isSoundDecoded('clickmusic')){
                clickmusic.play();
            }

        }
    },
};