var gameMenu = function(game) {};


gameMenu.prototype = {

    menuConfig: {
        startY: 260,
        startX: 30
    },

    init: function () {

    this.titleText = game.make.text(game.world.centerX, 0, "Game Title", {
        font: 'bold 50pt TheMinion',
        fill: '#ff8000',
        align: 'center',
        stroke: "green",
        strokeThickness : 10,
    });
    this.titleText.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
    this.titleText.anchor.set(0.5);
    game.add.tween(this.titleText).to( { y: 200 }, 2000, Phaser.Easing.Bounce.Out, true);

    this.playText = game.make.text(game.world.centerX, game.world.centerY, "Play", {
        font: 'bold 50pt TheMinion',
        fill: '#ff9f00',
        align: 'center',
        stroke: "#000000",
        strokeThickness : 5,
    });
    this.playText.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
    this.playText.anchor.set(0.5);
    this.playText.inputEnabled = true;
    game.add.tween(this.playText.scale).to({ x: 0.8, y: 0.8}, 700, Phaser.Easing.Linear.None, true, 500, 20, true);    
    
    this.playText.events.onInputOver.add(this.strokeColorChange, this);
    this.playText.events.onInputDown.add(this.strokeColorChange, this);
    this.playText.events.onInputUp.add(this.strokeColorOriginal, this);
    this.playText.events.onInputUp.add(this.clickMusic, this);
    this.playText.events.onInputUp.add(function () {
        game.state.start("gameStages");
    });

    this.exitText = game.make.text(game.world.width - 20, 30, "X", {
        font: 'bold 30pt TheMinion',
        fill: '#ffffff',
        align: 'center',
        stroke: "#000000",
        strokeThickness : 5,    
    });
    this.exitText.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
    this.exitText.anchor.set(0.5);
    this.exitText.inputEnabled = true;
    this.exitText.events.onInputOver.add(this.strokeColorChange, this);
    this.exitText.events.onInputDown.add(this.strokeColorChange, this);
    this.exitText.events.onInputUp.add(this.strokeColorOriginal, this);
    this.exitText.events.onInputUp.add(this.clickMusic, this);
    this.exitText.events.onInputUp.add(function () {
        if(confirm("Are you sure Want to close?")){
            window.close();
        }
    });
    this.exitText.alpha = 0;
    game.add.tween(this.exitText).to( { alpha: 1 }, 2000, "Linear", true);   
        
    var graphics = this.add.graphics(0, 0);
    graphics.lineStyle(5, 0xffffff, 1);

    // Drawing buttons sound, info, settings, score
    //Score
    graphics.beginFill(0x000000, 2);
    graphics.drawCircle(game.world.width - 65, game.world.height -65 , 100);
    this.scoreIcon = this.add.sprite(game.world.width - 112, game.world.height - 115, 'icons', 25);
    this.scoreIcon.alpha = 0;
    game.add.tween(this.scoreIcon).to( { alpha: 1 }, 2000, "Linear", true);
    this.scoreIcon.inputEnabled = true;
    this.scoreIcon.events.onInputUp.add(this.clickMusic, this);
    this.scoreIcon.events.onInputUp.add(function () {
        game.state.start("scores");
    });
    
    //Sound
    graphics.drawCircle(65, game.world.height -300 , 100);
    if( gameOptions.playMusic ){
        this.soundIcon = this.add.sprite(15, game.world.height -350, 'icons', 86);
    }else{
        this.soundIcon = this.add.sprite(15, game.world.height -350, 'icons', 92);
    }
    this.soundIcon.alpha = 0;
    game.add.tween(this.soundIcon).to( { alpha: 1 }, 2000, "Linear", true);
    this.soundIcon.inputEnabled = true;
    this.soundIcon.events.onInputUp.add(this.clickMusic, this);
    this.soundIcon.events.onInputDown.add(this.changeSoundIcon, this);
    
    //Credits
    graphics.drawCircle(65, game.world.height -180 , 100);
    this.creditsIcon = this.add.sprite(15, game.world.height -230, 'icons', 87);
    this.creditsIcon.alpha = 0;
    game.add.tween(this.creditsIcon).to( { alpha: 1 }, 2000, "Linear", true);
    this.creditsIcon.inputEnabled = true;
    this.creditsIcon.events.onInputUp.add(this.clickMusic, this);
    this.creditsIcon.events.onInputUp.add(function () {
        game.state.start("credits");
    });
   
    //Settings
    graphics.drawCircle(65, game.world.height -65 , 100);
    this.settingIcon = this.add.sprite(15, game.world.height -115, 'icons', 105);
    this.settingIcon.alpha = 0;
    game.add.tween(this.settingIcon).to( { alpha: 1 }, 2000, "Linear", true);
    this.settingIcon.inputEnabled = true;
    this.settingIcon.events.onInputUp.add(this.clickMusic, this);
    this.settingIcon.events.onInputUp.add(function () {
        game.state.start("settings");   
    });
    
    this.optionCount = 1;
    },
    strokeColorChange: function(item){
        item.stroke = "green";
    },
    strokeColorOriginal: function(item){
        item.stroke = "#000000";
    },
    create: function () {

    if (music.name !== "menumusic" && playMusic) {
        music.stop();
        music = game.add.audio('menumusic');
        music.loop = true;
        music.play();
    }

    game.add.existing(this.titleText);
    game.add.existing(this.playText);
    game.add.existing(this.exitText);
    
    game.state.add('credits', credits);
    game.state.add('settings', settings);
    game.state.add('scores', scores);
    game.state.add('gameStages', gameStages);
    
    },
    clickMusic: function(){
        clickmusic.stop();
        if(gameOptions.playSound == true){
            if (game.cache.isSoundDecoded('clickmusic')){
                clickmusic.play();
            }

        }
    },
    changeSoundIcon: function(){
        if( gameOptions.playMusic ){
            music.pause();
            gameOptions.playMusic = false;
            this.soundIcon.frame = 92;
        }else{
            music.resume();
            gameOptions.playMusic = true;
            this.soundIcon.frame = 86;
        }
        
    },
    update: function(){
        
    },
};