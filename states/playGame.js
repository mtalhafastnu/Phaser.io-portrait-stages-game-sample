var playGame = function(game) {};

playGame.prototype = {
    init: function () {
        this.pauseText = game.make.text(game.world.centerX + game.world.centerX - 140, 20, "Pause",     {
            font: 'bold 25pt TheMinion',
            fill: '#ffffff',
            align: 'left',
            stroke: "#000000",
            strokeThickness : 5,
        });
        this.pauseText.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
        this.pauseText.inputEnabled = true;
        this.pauseText.events.onInputUp.add(this.managePause, this);
        this.pauseText.events.onInputUp.add(this.clickMusic, this);
        
        this.backIcon = this.add.sprite(0, 0, 'icons', 66);
        this.backIcon.inputEnabled = true;
        this.backIcon.events.onInputUp.add(this.clickMusic, this);
        this.backIcon.events.onInputUp.add(function () {
            game.state.start("gameStages");   
        });

    },
    create: function () {
        game.add.existing(this.pauseText);
        
        var randstars = this.game.rnd.integerInRange(1, 3);
        // set nr of stars for this level
	PLAYER_DATA[gameOptions.gameStage - 1] = randstars;
        // unlock next level
        if (gameOptions.gameStage < PLAYER_DATA.length) {
            if (PLAYER_DATA[gameOptions.gameStage] < 0) { // currently locked (=-1)
                PLAYER_DATA[gameOptions.gameStage] = 0; // set unlocked, 0 stars
            }
        };
        
        window.localStorage.setItem('phaser_game_player_data', JSON.stringify(PLAYER_DATA));
        
        console.log(PLAYER_DATA);
        console.log(randstars);
        console.log(gameOptions.gameStage);
        
    },
    clickMusic: function(){
        clickmusic.stop();
        if(gameOptions.playSound == true){
            if (game.cache.isSoundDecoded('clickmusic')){
                clickmusic.play();
            }
        }
    },
    managePause: function() {
        game.paused = true;

        this.gamePauseText = game.make.text(game.world.centerX, game.world.centerY, "Resume",     {
            font: 'bold 35pt TheMinion',
            fill: '#ffffff',
            align: 'left',
            stroke: "#000000",
            strokeThickness : 5,
        });
        this.gamePauseText.setShadow(5, 5, 'rgba(0,0,0,0.5)', 5);
        this.gamePauseText.anchor.set(0.5);
        this.gamePauseText.inputEnabled = true;
        game.add.existing(this.gamePauseText);
        //game.add.tween(this.gamePauseText.scale).to({ x: 0.8, y: 0.8}, 700, Phaser.Easing.Linear.None, true, 500, 20, true);

        //game resume function  
        game.input.onDown.add(function(){
            this.gamePauseText.destroy();
            game.paused = false;
        }, this);
    },
};